/*
	Node.js
	- Benefits - performance / familiarity / access to node
	- Nodes limitation - nodesjs does not have direct support for common web develpment
		- handling HTTP verb GET, PUSH, PUT, DELETE etc
		- Handling request different url paths routing
		- Serving of static files
	- This means that using Node.js on its own for web development may get tedious
	- Instead of wring the code for these common web developen task yourself

	What is Express.js
	- "A framework in porogramming ios a tool that provides ready made compoment or solln that are customized in order to speed up development"
	from netsolutions.com
	- An unopinioted (tayo ang gumagawa/gumagamit however andyan ung mga suln. i access na lang) framework does not tell the developer how to use it. AN opinionated framework on the hand has its own way of doing things that a developer has to follow
		- unopinionated framework merely gives you the tools and components and lets you freely structure your app
		- opinionated frameworks provide you with a file structure / scaffolding that a dev has to learn how to use
	- Rememebnt that noode js is part of express
	Opinionated web framework
		- the framework dictates how it should be used by the dev
		- speeds up the startup process of app dev
		- enforce best practices for the framework uses case
		- lack of flexibility could be drawback
	Unopinionated web framework
		- the dev dicatates how to use the frameworkd
		- offers flexibitlity to create applicatioj unbound by any use case
		- no right wayy
	Express.js
	- An open source, unipionated web application framework written in js and hosted in Node.JS
	- Advantages - over plain node.js
		- simplicity makes it ez to learn and use
		- offers ready to use components for the most common web dev needs
		- adds easier routing and processing of HTTP methods
*/
/*
	npm init -y
	npm install express
	npm install mongoose

	touch .gitignore - to ignore a file to be uploaded 
*/
// before the apps to run we have some dependencies
// Load the express.js module into our application and saved it in variable called express

const express = require("express");
const port = 4000;

// app is our server
// it will create an application that uses express and stores it as app
const app = express();

// Middleware (request handlers)
// express.json() is a method which allow us to handle the streaming of data and automatically parse the incoming JSON from our request.body

app.use(express.json()); // request handler / lahat ng request andito na, and converted na xa into JavaScript

// mock data
let users = [
	{
		username: "TStark3000",
		email: "starkindustries@mail.com",
		password: "notPeterParker"
	},
	{
		username: "ThorThunder",
		email: "thorStrongestAvenger@mail.com",
		password: "iLoveJane"
	}
]

let items = [
	{
		name: "Mjolnir",
		price: 50000,
		isActive: true
	},
	{
		name: "Vibranium Shield",
		price: 70000,
		isActive: true
	}
]

app.get("/", (request, response) => { // gumagamit lang tau ng curly braces pag may irerereturn tau
	response.send("Hello from my first ExpressJS-API")
	}
)

/*
	Mini Activity: 5 mins

	>> Create a get route in Expressjs which will be able to send a message in the client:

		>> endpoint: /greeting
		>> message: 'Hello from Batch230-surname'

	>> Test in postman
	>> Send your response in hangouts
*/

app.get("/greeting", (request, response) => { // gumagamit lang tau ng curly braces pag may irerereturn tau
	response.send("Hello from Batch230-surname")
	}
)

// retrieval of users in mock database
app.get("/users", (req, res) => {
	res.send(users)
});

app.get("/items", (req, res) => {
	res.send(items)
});

// from the body of the postman "obeject" - this JSON object is convert to app.use express.json into traditional object, ang ginawa natin na nagcocontent na ng paramenter, at nilagay nalang natin using push at dot method

app.post("/users", (request, response) => { 
	let newUser = {
		username:request.body.username,
		email: request.body.email,
		password: request.body.password
	}
	users.push(newUser);
	console.log(users);

	response.send(users);
});

app.put("/users/:index", (req, res) => {
	console.log(req.body);

	console.log(req.params); // index: '1'
	let index = parseInt(req.params.index); // kaya tau nag parseInt kasi naka string ung index "index '1", para maaccess at mapalitan natin ng value

	users[index].password = req.body.password;
	res.send(users[index]);
})

// delete the last documents in an array using pop
app.delete("/users", (req, res) => {
	users.pop()
	res.send(users)
});

/*
 let items = [
	{
		name: "Mjolnir",
		price: 50000,
		isActive: true
	},
	{
		name: "Vibranium Shield",
		price: 70000,
		isActive: true
	}
]
*/

// Activity
/*
	>> Create a new collection in postman called s34-activity
	>> Save the postman collection in your s34 folder
	>> Create a new route to get and send items array in the client (get all items)
*/

// [GET]
// >> Create a new route to get and send items array in the client (GET ALL ITEMS)
// Insert your code here..

app.get("/items", (req, res) => {
	res.send(items)
});

// [POST]
// >> Create a new route to create and add a new item object in the items array (CREATE ITEM)
// >> Send the updated items array in the client
// >> Check if the post method route for our users for reference

app.post("/items", (request, response) => { 
	let newItem = {
		name:request.body.name,
		price: request.body.price,
		isActive: request.body.isActive
	}
	items.push(newItem);
	console.log(items);

	response.send(items);
});

// [PUT]
// >> Create a new route which can update the price of a single items in the array (UPDATE ITEM)
// >> Pass the index number of the item that you want to update in the request params (include a index number to the URL)
// >> add the price update update in the request body
// >> reassign the new price from our request body
// >> send the updated item to the client

app.put("/items/:index", (req, res) => {
	console.log(req.body);

	console.log(req.params); 
	let index = parseInt(req.params.index); 

	items[index].price = req.body.price;
	res.send(items[index]);
})


// app.listen(port, () => console.log(`Server is running at port ${port}`)); 
app.listen(port, () => console.log("Server is running at port " + port)); 

// pagmagsusubmit tau sa body using post man, use body>raw>JSON
// take note to type in exit on your gitbash, there may be a times that your app is still running eventhou your gitbash is closed
// whenever we add from post we need to make sure that we configure body of the post>>raw>JSON